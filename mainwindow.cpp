#include "mainwindow.h"
#include <QtGui>
#include <QtWidgets>
#include <QIODevice>
void SerialIO::choosePort(QIODevice::OpenMode mode)
{
    QList<QSerialPortInfo> liste=QSerialPortInfo::availablePorts();
    if(liste.isEmpty()){
        QMessageBox::information(0,"Doof!","Tut mir leid, aber ich finde keine seriellen Schnittstellen\n"
                                 "Zugriffsrechte?");
        return;
    }
    QDialog dial;
    QVBoxLayout*lay=new QVBoxLayout(&dial);
    QButtonGroup * bg=new QButtonGroup(&dial);
    bg->setExclusive(true);
    lay->addWidget(new QLabel("Wähle den seriellen Port!"));
    for (int i=0;i<liste.count();i++){
      QSerialPortInfo port=liste.at(i);
      qDebug()<<"Port "<<port.portName()<<" Description: "<<port.description();
      QCheckBox * cb=new QCheckBox(port.portName()+": "+port.description());
      bg->addButton(cb,i);
      lay->addWidget(cb);
    }
    QPushButton*okButton=new QPushButton("Ok",&dial);
    lay->addWidget(okButton);
    QObject::connect(okButton,&QAbstractButton::clicked,&dial,&QDialog::accept);
    dial.setLayout(lay);
    QSerialPortInfo info;
    if(dial.exec()==QDialog::Accepted){
        info=liste.at(bg->checkedId());
    }
    if(port!=nullptr){
        if(port->isOpen())
          port->close();
        delete port;
    }
    port=new QSerialPort(info);
    portinfo=info;
    connect(port,SIGNAL(baudRateChanged(qint32,QSerialPort::Directions)),this,SIGNAL(baudRateChanged(qint32,QSerialPort::Directions)));
    connect(port,SIGNAL(readyRead()),this,SIGNAL(readyRead()));
    QObject::connect(port,&QObject::destroyed,[this](){port=nullptr;portinfo=QSerialPortInfo();
        portStatusPix=QPixmap(":graysmallLED.png");
        portStatusString="kein Port gewählt";
        emit portStatusChanged();
        emit baudRateChanged(0,QSerialPort::AllDirections);
    });
    QObject::connect(port,&QSerialPort::errorOccurred,[this](QSerialPort::SerialPortError error){
        if(error!=0)
        QMessageBox::warning(0,"So ein Mist",QString("Es ist der Fehler %1 mit dem Port aufgetreten: \n")+port->errorString());
    });
    initialize(port,mode);

}

void SerialIO::openPort(QIODeviceBase::OpenMode mode)
{
   if(port!=nullptr ){
       //port->open(QIODevice::ReadOnly);
       initialize(port,mode);
   }else{
       choosePort(mode);
   }
   if(port)
       emit baudRateChanged(port->baudRate(),QSerialPort::AllDirections);
}

void SerialIO::closePort()
{
   if(port!=nullptr){
       if(port->isOpen())
          port->close();
       portStatusPix=QPixmap(":redsmallLED");
       portStatusString=QString("Port %1 geschlossen").arg(portinfo.portName());
       emit portStatusChanged();
   }
}
void SerialIO::setBaudrate(qint32 baudrate){
    QSettings settings;
    settings.setValue("baudrate",baudrate);
    if(port){
        port->setBaudRate(baudrate);
    }
    emit baudRateChanged(baudrate,QSerialPort::AllDirections);
}

void SerialIO::initialize(QSerialPort *port,QIODevice::OpenMode mode)
{
    QSettings settings;
    if(!settings.contains("serialport")){
        QMessageBox::warning(0,"Schade","RP6 oder Arduino? Rufe den Einstellungsdialog auf!\n"
                             "Nehme desweiteren an: Arduino mit Baudrate 19200");
        settings.setValue("serialport","Arduino");
        settings.setValue("baudrate",19200);
    }
    if(settings.value("serialport","Arduino").toString()=="RP6"){
        port->setBaudRate(QSerialPort::Baud38400);
        port->setDataBits(QSerialPort::Data8);
        port->setParity(QSerialPort::NoParity);
        port->setStopBits(QSerialPort::OneStop);
        port->setFlowControl(QSerialPort::NoFlowControl);
        if(port->open(QIODevice::ReadOnly))
           port->setRequestToSend(false);
    }else{
        //int baudrate=settings.value()
        port->setBaudRate(settings.value("baudrate",19200).toInt());
        port->open(mode);
    }
    if(port->isOpen()){
      portStatusPix=QPixmap(":greensmallLED");
      portStatusString=QString("Port %1 geöffnet").arg(portinfo.portName());
    }else{
        portStatusPix=QPixmap(":redsmallLED");
        portStatusString=QString("Port %1 geschlossen").arg(portinfo.portName());
    }
    emit portStatusChanged();
}

void SerialIO::send(const QString &string)
{
    if(port && port->isOpen() && port->isWritable())
        port->write(string.toUtf8());
}
MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
{
    QMenuBar*menubar=menuBar();
    QMenu*portMenu=new QMenu("Port");
    portMenu->addAction("Port wählen",[this](){serialio.choosePort(QIODevice::ReadWrite);});
    portMenu->addAction("Port öffnen",[this](){serialio.openPort(QIODevice::ReadWrite);},QKeySequence(Qt::CTRL|Qt::Key_O));
    portMenu->addAction("Port schließen",&serialio,&SerialIO::closePort,QKeySequence("Ctrl+X"));
    //portMenu->addMenu(baudrateMenu());
    menubar->addMenu(portMenu);
    menubar->addAction("clear",[this](){logger->clear();});
    QWidget*central=new QWidget;
    QVBoxLayout *lay=new QVBoxLayout;
    logger=new QPlainTextEdit("Ausgabe der seriellen Schnittstelle");
    logger->setReadOnly(true);
    lay->addWidget(logger);
    writer=new QLineEdit();
    writer->setToolTip("Diese Zeile wird an die serielle Schnittstelle geschrieben");
    QHBoxLayout * inner=new QHBoxLayout;
    inner->addWidget(new QLabel("senden: "));
    inner->addWidget(writer);
    lay->addLayout(inner);
    central->setLayout(lay);
    setCentralWidget(central);
    QLabel * picLabel=new QLabel();
    picLabel->setPixmap(serialio.portStatusPix);
    QLabel * stringLabel=new QLabel(serialio.portStatusString);
    statusBar()->addPermanentWidget(picLabel);
    statusBar()->addPermanentWidget(stringLabel);
    QPushButton* baudBut=new QPushButton(this);
    QMenu*baudMen=baudrateMenu();
    baudBut->setText(baudMen->title());
    connect(baudBut,&QPushButton::clicked,[baudMen](){
        baudMen->popup(QCursor::pos());
    });
    connect(&serialio,&SerialIO::baudRateChanged,[baudBut](qint32 rate,QSerialPort::Directions){
        baudBut->setText("Baud "+QString::number(rate));
    });
    statusBar()->addPermanentWidget(baudBut);
    connect(&serialio,&SerialIO::portStatusChanged,[this,stringLabel,picLabel](){
       stringLabel->setText(serialio.portStatusString);
       picLabel->setPixmap(serialio.portStatusPix);
    });
    connect(&serialio,&SerialIO::readyRead,[this](){
        while(serialio.port->canReadLine()){
            char buffer[240];
            serialio.port->readLine(buffer,240);
            QString zeile=buffer;
            zeile=zeile.replace("\r\n","");
            logger->appendPlainText(zeile);
        }
    });
    connect(writer,&QLineEdit::returnPressed,[this](){
          serialio.send(writer->text());
          writer->setText("");
    });
    writer->setFocus();

}

QMenu *MainWindow::baudrateMenu()
{
    QMenu*baudmenu=new QMenu;
    QActionGroup * group=new QActionGroup(baudmenu);
    qint32 defaultrate=QSettings().value("serialport","Arduino").toString()=="RP6"?38400:19200;
    for(qint32 baud:{2400,4800,9600,19200,31250,38400,57600,74880,250000}){
        QAction * act=new QAction(QString::number(baud));
        act->setCheckable(true);
        if(baud==defaultrate ) act->setChecked(true);
        //act->setData(baud);
        connect(act,&QAction::triggered,[this,baud,baudmenu](bool checked){
            if(checked) {serialio.setBaudrate(baud);
                baudmenu->setTitle("Baud: "+QString::number(baud));
            }
        });
        group->addAction(act);
        baudmenu->addAction(act);
    }
    group->setExclusive(true);
    QString titlestring="Baudrate ";
    titlestring+=QString::number(defaultrate);
    baudmenu->setTitle(titlestring);
    return baudmenu;
}

MainWindow::~MainWindow()
{
}

