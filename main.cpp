#include "mainwindow.h"

#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    QCoreApplication::setApplicationName("Arduino2Serial");
    QCoreApplication::setOrganizationName("UBoss");
    MainWindow w;
    w.show();
    return a.exec();
}
