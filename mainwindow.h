#ifndef MAINWINDOW_H
#define MAINWINDOW_H
#include <QMainWindow>
#include <QSerialPort>
#include <QSerialPortInfo>
class QPlainTextEdit;
class QLineEdit;
class SerialIO:public QObject{
    Q_OBJECT
public:
    SerialIO():QObject(){}
    void choosePort(QIODeviceBase::OpenMode mode);//hier kann man den Port wählen.
    void openPort(QIODevice::OpenMode mode=QIODevice::ReadOnly);//öffnet den gewählten Port.
    void closePort();//schließt den gewählten Port, der kann aber wieder geöffnet werden.

    QSerialPort * port=nullptr;
    QSerialPortInfo portinfo;
    void initialize(QSerialPort*port, QIODeviceBase::OpenMode mode=QIODevice::ReadOnly);
    void setBaudrate(qint32 rate);
    void send(const QString&string);
    QPixmap portStatusPix=QPixmap(":graysmallLED");
    QString portStatusString="kein Port geöffnet";
    //bool logData=true;
    //Stopwatch watch;
signals:
    void portStatusChanged();
    void baudRateChanged(qint32,QSerialPort::Directions);
    void readyRead();
};
class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    QMenu*baudrateMenu();
    QPlainTextEdit * logger;
    QLineEdit * writer;
    SerialIO serialio;
    ~MainWindow();
};
#endif // MAINWINDOW_H
